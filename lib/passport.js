const passport = require('passport');
// const LocalStrategy = require('passport-local').Strategy
const {
  Strategy: JwtStrategy,
  ExtractJwt
} = require('passport-jwt')
const {
  User
} = require('../models')

/* Passport JWT Options */
const options = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: 'Ini cawan dan rahasia',
}
passport.use(new JwtStrategy(options, async (payload, done) => {
  User.findByPk(payload.id)
    .then(user => done(null, user))
    .catch(err => done(err, false))
}))

module.exports = passport;