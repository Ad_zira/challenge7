const {
  Room
} = require('../models');
const AppError = require('../utils/appError');
const auth = require('./authController')
// const passport = require('../lib/passport');

const roomController = {
  createRoom: async (req, res) => {
    if (!auth.loginJwt) {
      res.send("Please login first")
    }
    // const roomId = req.params.id; //sepertinya di sini logicnya masih belum benar..
    const makeRoom = await Room.create({
        roomname: req.body.roomname,
        player1: req.user.player1
      })
      .then(() => {
        res.json({
          message: `Success making a room with id: ${roomId}`
        })
      })
      .catch(err => {
        res.send('error', err.message);
      });
    return makeRoom;
  },
  toRoom: async (req, res) => {
    try {
      // "http://localhost:3000/api/v1/room/:id"
      const roomId = req.params.id;

      const enterRoom = await Room.findOne({
        where: {
          id: roomId,
          roomname
        }
      });
      if (!roomId) {
        return next(new AppError('No room found with that ID.'))
      }
      return enterRoom;
    } catch (error) {
      res.status(501).json({
        message: "Sorry, please try again later ^_^"
      })
    }
  },
  updateRoom: async (req, res, next) => { //only updating room name
    try {
      const updatingRoom = await Room.build(req.room.id, {
        roomname: req.body.roomname
      })
      updatingRoom.save();
      if (!updatingRoom) {
        return next(new AppError("No room found with that ID."))
      }
      res.status(201).send('Yeay, the room-name is updated!')
    } catch (error) {
      res.send('Error', error.message);
    }
  },
  deleteRoom: async (req, res, next) => {
    // const roomId = Room.findOne({
    //   where: {
    //     id: roomId
    //   }
    // })
    try {
      const roomId = await Room.destroy({
        where: {
          roomId: req.params.id
        }
      });
      if (!roomId) {
        return next(new AppError("No room found with that ID!", 404))
      }
      res.status(204).json({
        message: "Room deleted",
        data: null
      });
    } catch (err) {
      res.send('Error', err.message);
    }
  }
}

module.exports = roomController;