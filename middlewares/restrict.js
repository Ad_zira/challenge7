// Deprecated local strategy
// module.exports = (req, res, next) => {
//   if (req.isAuthenticated()) return next();
//   res.redirect('/user/login')
// }

// JWT
const passport = require('../lib/passport');
module.exports = passport.authenticate('jwt', {
  session: false
})