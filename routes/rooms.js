const router = require('express').Router();
const {
  toRoom,
  createRoom,
  updateRoom,
  deleteRoom
} = require('../controllers/roomController');
const {
  restrictTo
} = require('../controllers/authController')
const {
  ongoingMatch,
  releaseScore,
  gameHistory
} = require('../controllers/matchController');
const restrict = require('../middlewares/restrict');

/* ROOM MANIPULATION */

// ('/api/v1/rooms')
// router.post('/api/v1/room/create', restrict, createRoom);
// router.get('/api/v1/room/:id', restrict, toRoom);

// non-('/api/v1/rooms') routes ^,^v
// Just add a thing
router.post('/create', restrict, createRoom);
router.get('/:id', restrict, toRoom);
router.route('/:id').get(toRoom).patch(updateRoom);

// Admin's authorization
router.use(restrictTo('admin'))
// router.route('/api/v1/room/:id').delete(restrict, deleteRoom)
router.route('/:id').delete(restrict, deleteRoom)



/* GAME MANIPULATION */
router.post('/:id/choice', ongoingMatch);
router.get('/:id/result', releaseScore); // Winning amount
router.get('/history', gameHistory); // (which roomId was used, who-won-who-lost) data


module.exports = router;