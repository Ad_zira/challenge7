'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.Match, {
        // as: "chamber",
        foreignKey: "RoomId"
      })
      this.belongsTo(models.User)
    }

  };
  Room.init({
    player1: DataTypes.STRING,
    player2: DataTypes.STRING,
    roomname: {
      type: DataTypes.STRING,
      allowNull: false
    },
    choice: DataTypes.STRING,
    match: DataTypes.INTEGER
  }, {
    sequelize,
    tableName: 'Rooms',
    modelName: 'Room',
  });
  return Room;
};