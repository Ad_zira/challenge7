'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Match extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.User);
      this.belongsTo(models.Room);
      this.hasOne(models.Room);
    }

    matchOngoing() {

      function play() {
        choose({
            title: "Silakan Pilih",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            inputPlaceholder: "Pilith antara Kertas, gunting, dan Batu."
          },
          function (inputValue) {

            let player1 = {
              choice1: "Gunting",
              choice2: "Kertas",
              choice3: "Batu"
            };
            let player2 = {
              choice1: "Gunting",
              choice2: "Kertas",
              choice3: "Batu"
            }
            // let comp = Math.floor((Math.random() * 3) + 1);
            // let comp = Math.floor((Math.random() * suit.length) + 1);

            if (player2 === 1) {
              player2 = "Kertas";
            } else if (player2 === 2) {
              player2 = "Batu";
            } else {
              player2 = "Gunting";
            }
            if (inputValue === false) return false;

            if (inputValue === "") {
              choose.showInputError("Pilihan anda kosong");
              return false;
            } else if (inputValue === "Batu") {
              hasil = player2 === "Kertas" ? "Anda Kalah" : "Anda Menang";
            } else if (inputValue === player2) {
              hasil = "Seri!"
            } else if (inputValue === "Kertas") {
              hasil = player2 === "gunting" ? "Anda Kalah" : "Anda Menang";
            } else if (inputValue === "Gunting") {
              hasil = inputValue === "Batu" ? "Anda Kalah" : "Anda Menang"
            } else {
              choose("Harap Masukkan Gunting, Batu, atau Kertas saja")
            }

            choose("Hasilnya adalah " + hasil + ", lawan memilih " + player2 + ", Dan Anda memilih " + inputValue)
          })
      }
      play();
    }

  };
  Match.init({
    userId: DataTypes.INTEGER,
    roomId: DataTypes.INTEGER,
    status: DataTypes.STRING,
    choose: DataTypes.STRING
  }, {
    sequelize,
    tableName: 'Matches',
    modelName: 'Match',
  });
  return Match;
};