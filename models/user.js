'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasOne(models.Room);
      this.hasMany(models.Match, {
        as: "history",
        foreignKey: "UserId"
      })
    }

    toJSON() {
      return {
        ...this.get(),
        accessToken: undefined,
      }
    }

    static encrypt = (password) =>
      bcrypt.hashSync(password, 10)

    static register = ({
      username,
      password
    }) => {
      console.log('username: ', username)
      // console.log('password: ', password)
      const encryptedPassword = this.encrypt(password)
      return this.create({
        username,
        password: encryptedPassword
      })
    }

    checkPassword = (password) => bcrypt.compareSync(password, this.password);

    static authenticate = async ({
      username,
      password
    }) => {
      try {
        const user = await this.findOne({
          where: {
            username
          }
        });
        if (!user) throw new Error('User not found!');

        const isValidPassword = user.checkPassword(password);
        if (!isValidPassword) return Promise.reject('Wrong password');
        return Promise.resolve(user);

      } catch (err) {
        return Promise.reject(err);
      }
    };

    generateToken = () => {
      const payload = {
        id: this.id,
        username: this.username,
      }
      const options = {
        expiresIn: "24h"
      }
      const secret = 'Ini cawan dan rahasia';
      const token = jwt.sign(payload, secret, options);
      return token;
    }
  };
  User.init({
    username: {
      type: DataTypes.STRING,
      unique: true,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        // is: /^\w{8,20}$/,
        is: /^\w+$/,
        len: [8, 20]
      }
    },
    role: {
      type: DataTypes.ENUM,
      values: ['admin', 'player'],
      defaultValue: 'player'
    }
  }, {
    sequelize,
    // tableName: 'users'
    tableName: 'Users',
    modelName: 'User',
  });
  return User;
};