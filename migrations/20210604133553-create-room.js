'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Rooms', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      player1: {
        type: Sequelize.STRING,
        references: {
          model: {
            tableName: 'Users',
          },
          key: 'id'
        }
      },
      player2: {
        type: Sequelize.STRING,
        references: {
          model: {
            tableName: 'Users'
          },
          key: 'id',
        }
      },
      roomname: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      choice: {
        type: Sequelize.STRING
      },
      match: {
        type: Sequelize.INTEGER,
        // Match requires 3 games, whoever makes 2 (scores) first, wins
        references: {
          model: {
            tableName: 'Matches',
          },
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Rooms');
  }
};